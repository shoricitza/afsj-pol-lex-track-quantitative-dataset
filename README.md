## Dataset description

The AFSJ-Pol-Lex-Track dataset contains quantitative and qualitative information about the legislative process in the Area of Freedom, Security and Justice from the entry into force of the Amsterdam Treaty until December 2017. It aims to tack the evolution of AFSJ legislative proposals from the text proposed by the European Commission to the final adopted act or withdrawn proposal. 

The raw data is extracted through web scrapping from [EUR-lex](https://eur-lex.europa.eu/homepage.html), [OEIL](https://oeil.secure.europarl.europa.eu/oeil/home/home.do) and [Council's register](https://www.consilium.europa.eu/en/documents-publications/public-register/), using the inter-institutional code as the common reference.

The dataset contains two types of information:

1. Quantitative information about each legislative procedure
2. Text data of the positions of the European Commission, the European Parliament (EP) and the Council of Ministers 

**The quantitative dataset** can be accessed [here](https://gitlab.com/shoricitza/afsj-pol-lex-track-quantitative-dataset/-/blob/master/AFSJPolLexTrack-_qualitative_database_final.csv).

The dataset contains general information about the legal act (legal basis, title of the legislative act, interinstitutional code, CELEX number, type of procedure, type of act, directory codes and subcodes, total duration of the procedure in number of days etc.), as well as information about the procedure in each institution (EP votes, names and party affiliation of the rapporteurs, rapporteurs for opinion, dates of position at 1st reading, 2nd reading, 3rd reading, date of the Council’s political agreement, Council’s position at 1st reading, 2nd reading, 3rd reading, number of points A and B on the agenda of the Council, responsible DG, position of the Commission on EP’s amendments at each reading etc.). 

**The codebook** can be accessed [here](https://gitlab.com/shoricitza/afsj-pol-lex-track-quantitative-dataset/-/blob/master/Codebook.pdf). 


